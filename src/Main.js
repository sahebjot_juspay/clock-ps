exports.getNativeTime = function() {
    var now = new Date()
    return {h:now.getHours(), m: now.getMinutes(), s: now.getSeconds()}
}
