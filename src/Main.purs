module Main where

import Prelude

import Control.Monad.Eff (Eff, kind Effect)
import Control.Monad.Eff.Console (CONSOLE, log)
import Control.Monad.Eff.Timer (TIMER, setInterval)
import Data.Array (zip, zipWithA, (..))
import Data.Int (toNumber)
import Data.Maybe (Maybe(..))
import Data.Tuple (Tuple(..))
import Graphics.Canvas (Arc, CANVAS, Context2D, arc, beginPath, clearRect, closePath, fillText, getCanvasElementById, getContext2D, lineTo, measureText, moveTo, setLineWidth, setStrokeStyle, stroke)
import Math (cos, pi, sin)

-- Any time related function talking to the native side should have this effect
foreign import data NOW :: Effect

-- Native function that get's the time record from javascript
foreign import getNativeTime :: forall eff. Eff (now :: NOW | eff) CurrentTime

-- Record that maps to the Javascript object
type CurrentTime = {h :: Int, m :: Int, s :: Int}

data Time = Time Int Int Int

-- Center of the clock
clockCenter :: {x:: Number, y:: Number}
clockCenter = {x: 150.0, y: 150.0}

-- Clock outer circle
clockArc :: Arc
clockArc = {x: clockCenter.x, y: clockCenter.y, r: 100.0, start:0.0, end: (2.0 * pi)}

-- Drawing the outer circle of the clock
drawOuterCircle :: forall eff. Context2D -> Eff (canvas :: CANVAS | eff) Context2D
drawOuterCircle ctx = do
  _ <- beginPath ctx
  _ <- arc ctx clockArc
  _ <- setStrokeStyle "#000000" ctx
  _ <- setLineWidth 1.0 ctx
  _ <- closePath ctx
  stroke ctx

-- Drawing all of the clock
drawClock :: forall eff. Context2D -> Eff (canvas :: CANVAS, now :: NOW, console :: CONSOLE | eff) Context2D
drawClock ctx = do
  _ <- drawOuterCircle ctx
  currentTime <- getTime
  _ <- drawHours ctx
  drawHands currentTime ctx

-- Draw the hands of the clock
drawHand :: forall eff. Context2D -> Number -> Number -> Eff (canvas :: CANVAS | eff) Context2D
drawHand ctx x y = do
  _ <- beginPath ctx
  _ <- moveTo ctx clockCenter.x clockCenter.y
  _ <- lineTo ctx x y
  _ <- closePath ctx
  stroke ctx

-- Write all the hours for the clock
drawHours :: forall eff. Context2D -> Eff (canvas :: CANVAS, console :: CONSOLE | eff) Context2D
drawHours ctx = do
  -- Getting character size to leave some space from the outer circle
  tm <- (measureText ctx "1")
  let hours = map toNumber (-2..9)
      hourAngleGap = (2.0 * pi) / 12.0
      radiusOffset = 0.85
      offset = tm.width
      -- Getting the correct x,y coordinates for where the hour should be written
      xcoords = map (\hour -> clockCenter.x - offset + (clockArc.r * radiusOffset) * cos (hour * hourAngleGap)) hours
      ycoords = map (\hour -> clockCenter.y + offset + (clockArc.r * radiusOffset) * sin (hour * hourAngleGap)) hours
      zippedCoords = zip xcoords ycoords
      -- Writing the text for those coordinates
  _ <- zipWithA (\(Tuple x y) index -> fillText ctx (show index) x y) zippedCoords (1..12)
  pure ctx

-- Drawing the hands of the clock
drawHands :: forall eff. Time -> Context2D -> Eff (canvas :: CANVAS, console :: CONSOLE | eff) Context2D
drawHands (Time h m s) ctx = do
  let secondsAngleGap = (2.0 * pi) / 60.0
      minutesAngleGap = (2.0 * pi) / 60.0
      hoursAngleGap   = (2.0 * pi) / 12.0
      secondOffset    = 0.75
      minuteOffset    = 0.6
      hourOffset      = 0.45
      -- Converting the 24 hour clock to the 12 hour clock
      convertedHour   = toNumber $ mod h 12
      -- Set the hands of the clock with the correct angle based on the time
      -- val * angle offset will give the correct angle, where val is hour or minute or second
      sx              = clockCenter.x + (clockArc.r * secondOffset) * cos ((toNumber s) * secondsAngleGap - (pi / 2.0))
      sy              = clockCenter.y + (clockArc.r * secondOffset) * sin ((toNumber s) * secondsAngleGap - (pi / 2.0))
      mx              = clockCenter.x + (clockArc.r * minuteOffset) * cos ((toNumber m) * minutesAngleGap - (pi / 2.0))
      my              = clockCenter.y + (clockArc.r * minuteOffset) * sin ((toNumber m) * minutesAngleGap - (pi / 2.0))
      hx              = clockCenter.x + (clockArc.r * hourOffset) * cos (convertedHour * hoursAngleGap - (pi / 2.0))
      hy              = clockCenter.y + (clockArc.r * hourOffset) * sin (convertedHour * hoursAngleGap - (pi / 2.0))
  _ <- drawHand ctx sx sy
  _ <- drawHand ctx mx my
  drawHand ctx hx hy

-- Converting CurrentTime to Time Sum type
getTime :: forall eff. Eff (now :: NOW, console :: CONSOLE | eff) Time
getTime = do
  time <- getNativeTime
  pure $ Time time.h time.m time.s

-- Get canvas, context and start drawing the clock every 1 second
main :: forall e. Eff (console :: CONSOLE, canvas :: CANVAS, now :: NOW, timer :: TIMER | e) Unit
main = do
  maybeCanvasElem <- getCanvasElementById "canvas"
  case maybeCanvasElem of
       Just canvas -> do
          ctx <- getContext2D canvas
          _ <- setInterval 1000 do
             _ <- clearRect ctx {x: 0.0, y: 0.0, w: 800.0, h: 800.0}
             _ <- drawClock ctx
             pure unit
          log "clock started"
       Nothing     -> log "canvas element was not found"
